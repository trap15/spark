OBJECTS = spark.o arc_paa.o arc_par.o mdl_pmd.o mdl_ptr.o mdl_pme.o common.o
OUTPUT = spark
CFLAGS = -Wall -pedantic -g
LDFLAGS = -lz

all: $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	$(RM) $(OUTPUT) $(OBJECTS)
