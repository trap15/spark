/*
	SPARK - Dead or Alive Paradise Tool

Copyright (C) 2010              Giuseppe "The Lemon Man"
Copyright (C) 2010~2011         Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <zlib.h>

#include "common.h"
#include "arc_paa.h"
#include "arc_par.h"
#include "mdl_pmd.h"
#include "mdl_ptr.h"
#include "mdl_pme.h"

#define VERSION_MAJOR		0
#define VERSION_MINOR		3

int main(int argc, char *argv[])
{
	FILE* fp;
	u32 magic;
	int c;
	int ret = EXIT_FAILURE;
	app = argv[0];
	fprintf(stderr, 
		"SPARK v%d.%d - Dead or Alive Paradise Tool\n"
		"By The Lemon Man and trap15\n",
		VERSION_MAJOR, VERSION_MINOR);
	while((c = getopt(argc, argv, "v")) != -1) {
		switch(c) {
			case 'v':
				verbosity = 1;
				printf("Verbose mode\n");
				break;
			case '?':
				if(isprint(optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character \\x%x.\n", optopt);
				usage();
				return EXIT_FAILURE;
		}
	}

	if((optind + 1) < argc) {
		printf("%d %d\n", optind, argc);
		usage();
		return EXIT_FAILURE;
	}
	fp = fopen(argv[optind], "rb");
	if(fp == NULL) {
		fprintf(stderr, "Can't open %s\n", argv[1]);
		return EXIT_FAILURE;
	}
	READB32(magic, fp);
	while(!feof(fp)) {
		fseek(fp, -sizeof(u32), SEEK_CUR);
		switch(magic) {
			case MAGIC_PAA: /* PAA\0 */
				ret = paa_unarc(fp);
				break;
			case MAGIC_PAR: /* PAR\0 */
				ret = par_unarc(fp);
				break;
			case MAGIC_PMD: /* PMD\0 */
				ret = parse_pmd(fp);
				break;
			case MAGIC_PTR: /* PTR\0 */
				ret = parse_ptr(fp);
				break;
			case MAGIC_PME: /* PME\0 */
				ret = parse_pmd(fp);
				break;
			default:
				fprintf(stderr, "Unknown magic %08X\n", magic);
				ret = EXIT_FAILURE;
		}
		if(ret != EXIT_SUCCESS) {
			printf("Error reading file.\n");
			fseek(fp, 0, SEEK_END);
		}
		READB32(magic, fp);
	}

	fclose(fp);
	return ret;
}
