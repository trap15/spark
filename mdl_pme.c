/*
	SPARK - Dead or Alive Paradise Tool

Copyright (C) 2010              Giuseppe "The Lemon Man"
Copyright (C) 2010~2011         Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>

#include "common.h"
#include "mdl_pme.h"

typedef struct {
	u32 magic;
	u32 unk;
	u32 headersize;
	u32 nextoff;
	u32 tablecnt;
	u32 unk4;
	u32 unk5;
	u32 unk6;
} pme_file_t;

typedef struct {
	char name[0x20];
	u32 unk;
	u32 unk2;
	u32 unk3;
	u32 unk4;
} pme_data_t;

static FILE* doaPme;
static pme_file_t pme;
static u32* pmelist;
static u32 nextsecloc;
static pme_data_t* data;

static int read_header()
{
	int readdif;
	readdif = ftell(doaPme);
	READB32(pme.magic, doaPme);
	READ32(pme.unk, doaPme);
	READ32(pme.headersize, doaPme);
	READ32(pme.nextoff, doaPme);
	READ32(pme.tablecnt, doaPme);
	READ32(pme.unk4, doaPme);
	READ32(pme.unk5, doaPme);
	READ32(pme.unk6, doaPme);
	
	if(pme.magic != MAGIC_PME)
		return 1;
	
	fseek(doaPme, readdif, SEEK_SET);
	fseek(doaPme, pme.headersize, SEEK_CUR);
	if(verbosity > 0) {
		printf("PME:\n");
		printf("	Magic : %08X\n", pme.magic);
		printf("	Unk   : %08X\n", pme.unk);
		printf("	HdSz  : %08X\n", pme.headersize);
		printf("	NxtOff: %08X\n", pme.nextoff);
		printf("	TblCnt: %08X\n", pme.tablecnt);
		printf("	Unk4  : %08X\n", pme.unk4);
		printf("	Unk5  : %08X\n", pme.unk5);
		printf("	Unk6  : %08X\n", pme.unk6);
	}else{
		printf("Format incomplete; only output is through verbose mode at present.\n");
	}
	return 0;
}

static int read_offset_table()
{
	int i;
	u32 tmp;
	pmelist = calloc(pme.tablecnt + 1, sizeof(u32));
	for(i = 0; i < pme.tablecnt; i++) {
		READ32(pmelist[i], doaPme);
		printf("	[%04i/%04i] Offsets\n", i, pme.tablecnt);
		printf("		%08X\n", pmelist[i]);
	}
	for(i = 0; i < (pme.tablecnt + 3); i++) {
		READ32(tmp, doaPme);
		if(tmp == 0)
			nextsecloc = ftell(doaPme) - 4;
	}
	for(i = 0; i < pme.tablecnt; i++) {
		pmelist[i] += ftell(doaPme); /* Get direct offsets */
	}
	nextsecloc += pme.nextoff;
	pmelist[i] = nextsecloc - 4; /* kludge */
	return 0;
}

static int parse_data()
{
	int i;
	void* tmp_data;
	data = calloc(pme.tablecnt, sizeof(pme_data_t));
	for(i = 0; i < pme.tablecnt; i++) {
		printf("	[%04i/%04i] Data\n", i, pme.tablecnt);
		fseek(doaPme, pmelist[i], SEEK_SET);
		fread(data[i].name, 1, 0x20, doaPme);
		READ32(data[i].unk, doaPme);
		READ32(data[i].unk2, doaPme);
		READ32(data[i].unk3, doaPme);
		READ32(data[i].unk4, doaPme);
		printf("		%s @ %08X:\n", data[i].name, pmelist[i]);
		printf("			Unk   : %08X\n", data[i].unk);
		printf("			Unk2  : %08X\n", data[i].unk2);
		printf("			Unk3  : %08X\n", data[i].unk3);
		printf("			Unk4  : %08X\n", data[i].unk4);
		printf("			Extra data:\n");
		tmp_data = malloc(pmelist[i+1] - pmelist[i] - 0x30);
		fread(tmp_data, pmelist[i+1] - pmelist[i] - 0x30, 1, doaPme);
		hexdump(tmp_data, pmelist[i+1] - pmelist[i] - 0x30);
		free(tmp_data);
	}
	fseek(doaPme, nextsecloc, SEEK_SET);
	return 0;
}

int parse_pme(FILE* fp)
{
	doaPme = fp;
	
	if(read_header()) {
		perror("Can't read header");
		return EXIT_FAILURE;
	}
	
	if(read_offset_table()) {
		perror("Can't read offset table");
		return EXIT_FAILURE;
	}
	
	if(parse_data()) {
		perror("Can't read offset table");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
