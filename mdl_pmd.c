/*
	SPARK - Dead or Alive Paradise Tool

Copyright (C) 2010              Giuseppe "The Lemon Man"
Copyright (C) 2010~2011         Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>

#include "common.h"
#include "mdl_pmd.h"

typedef struct {
	u32 magic;
	u32 unk;
	u32 headersize;
	u32 unk2;
} pmd_file_t;

static FILE* doaPmd;
static pmd_file_t pmd;

static int read_header()
{
	int readdif;
	readdif = ftell(doaPmd);
	READB32(pmd.magic, doaPmd);
	READ32(pmd.unk, doaPmd);
	READ32(pmd.headersize, doaPmd);
	READ32(pmd.unk2, doaPmd);
	
	if(pmd.magic != MAGIC_PMD)
		return 1;
	
	fseek(doaPmd, readdif, SEEK_SET);
	fseek(doaPmd, pmd.headersize, SEEK_CUR);
	if(verbosity > 0) {
		printf("PMD:\n");
		printf("	Magic : %08X\n", pmd.magic);
		printf("	Unk   : %08X\n", pmd.unk);
		printf("	HdSz  : %08X\n", pmd.headersize);
		printf("	Unk2  : %08X\n", pmd.unk2);
	}else{
		printf("Format incomplete; only output is through verbose mode at present.\n");
	}
	return 0;
}

int parse_pmd(FILE* fp)
{
	doaPmd = fp;
	
	if(read_header()) {
		perror("Can't read header");
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}
