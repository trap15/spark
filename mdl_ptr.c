/*
	SPARK - Dead or Alive Paradise Tool

Copyright (C) 2010              Giuseppe "The Lemon Man"
Copyright (C) 2010~2011         Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>

#include "common.h"
#include "mdl_ptr.h"

typedef struct {
	u32 magic;
	u32 unk;
	u32 headersize;
	u32 nextoff;
	u32 tablecnt;
	u32 unk4;
	u32 unk5;
	u32 unk6;
} ptr_file_t;

typedef struct {
	char name[0x20];
	u32 unk;
	u32 unk2;
	u32 unk3;
	u32 unk4;
	float floats[55];
	u32 extra[4];
} ptr_data_t;

static char* floatnames[55][2] = {
	{ "X   ", "" }, { "Y   ", "" }, { "Z   ", "\n" },
	{ "3   ", "" }, { "4   ", "" }, { "5   ", "" },
	{ "6   ", "" }, { "7   ", "" }, { "8   ", "" },
	{ "9   ", "" }, { "10  ", "" }, { "11  ", "" },
	{ "12  ", "" }, { "13  ", "" }, { "14  ", "" },
	{ "15  ", "" }, { "16  ", "" }, { "17  ", "" },
	{ "18  ", "" }, { "19  ", "" }, { "20  ", "" },
	{ "21  ", "" }, { "22  ", "" }, { "23  ", "" },
	{ "24  ", "" }, { "25  ", "" }, { "26  ", "" },
	{ "27  ", "" }, { "28  ", "" }, { "29  ", "" },
	{ "30  ", "" }, { "31  ", "" }, { "32  ", "" },
	{ "33  ", "" }, { "34  ", "" }, { "35  ", "" },
	{ "36  ", "" }, { "37  ", "" }, { "38  ", "" },
	{ "39  ", "" }, { "40  ", "" }, { "41  ", "" },
	{ "42  ", "" }, { "43  ", "" }, { "44  ", "" },
	{ "45  ", "" }, { "46  ", "" }, { "47  ", "\n" },
	{ "X2  ", "" }, { "Y2  ", "" }, { "Z2  ", "\n" },
	{ "51  ", "" }, { "52  ", "" }, { "53  ", "" },
	{ "54  ", "" },
};

static ptr_file_t ptr;
static u32 *ptrlist;
static ptr_data_t *data;
static u32 nextsecloc;
static FILE* doaPtr;

static void print_ptr_floats(float floats[])
{
	int i;
	for(i = 0; i < 55; i++) {
		printf("				%s: %f\n%s", floatnames[i][0], floats[i], floatnames[i][1]);
	}
}

static int read_header()
{
	int readdif;
	readdif = ftell(doaPtr);
	
	READB32(ptr.magic, doaPtr);
	READ32(ptr.unk, doaPtr);
	READ32(ptr.headersize, doaPtr);
	READ32(ptr.nextoff, doaPtr);
	READ32(ptr.tablecnt, doaPtr);
	READ32(ptr.unk4, doaPtr);
	READ32(ptr.unk5, doaPtr);
	READ32(ptr.unk6, doaPtr);
	
	if(ptr.magic != MAGIC_PTR)
		return 1;
	
	fseek(doaPtr, readdif, SEEK_SET);
	fseek(doaPtr, ptr.headersize, SEEK_CUR);
	printf("PTR:\n");
	printf("	Magic    : %08X\n"
	       "	Unk      : %08X\n"
	       "	HeadSz   : %08X\n"
	       "	NextOff  : %08X\n"
	       "	TblCnt   : %08X\n"
	       "	Unk4     : %08X\n"
	       "	Unk5     : %08X\n"
	       "	Unk6     : %08X\n",
	       ptr.magic, ptr.unk, ptr.headersize, ptr.nextoff,
	       ptr.tablecnt, ptr.unk4, ptr.unk5, ptr.unk6);
	return 0;
}

static int read_offset_table()
{
	int i;
	u32 tmp;
	ptrlist = calloc(ptr.tablecnt + 1, sizeof(u32));
	for(i = 0; i < ptr.tablecnt; i++) {
		READ32(ptrlist[i], doaPtr);
		printf("	[%04i/%04i] Offsets\n", i, ptr.tablecnt);
		printf("		%08X\n", ptrlist[i]);
	}
	for(i = 0; i < (ptr.tablecnt + 3); i++) {
		READ32(tmp, doaPtr);
		if(tmp == 0)
			nextsecloc = ftell(doaPtr) - 4;
	}
	for(i = 0; i < ptr.tablecnt; i++) {
		ptrlist[i] += ftell(doaPtr); /* Get direct offsets */
	}
	nextsecloc += ptr.nextoff;
	ptrlist[i] = nextsecloc - 4; /* kludge */
	return 0;
}

static int parse_data()
{
	int i, l;
	data = calloc(ptr.tablecnt, sizeof(ptr_data_t));
	for(i = 0; i < ptr.tablecnt; i++) {
		printf("	[%04i/%04i] Data\n", i, ptr.tablecnt);
		fseek(doaPtr, ptrlist[i], SEEK_SET);
		fread(data[i].name, 1, 0x20, doaPtr);
		READ32(data[i].unk, doaPtr);
		READ32(data[i].unk2, doaPtr);
		READ32(data[i].unk3, doaPtr);
		READ32(data[i].unk4, doaPtr);
		for(l = 0; l < 55; l++) {
			READ32(data[i].floats[l], doaPtr);
		}
		for(l = 0; l < 4; l++) {
			READ32(data[i].extra[l], doaPtr);
		}
		printf("		%s @ %08X:\n", data[i].name, ptrlist[i]);
		printf("			Unk   : %08X\n", data[i].unk);
		printf("			Unk2  : %08X\n", data[i].unk2);
		printf("			Unk3  : %08X\n", data[i].unk3);
		printf("			Unk4  : %08X\n", data[i].unk4);
		printf("			Floats:\n");
		print_ptr_floats(data[i].floats);
		printf("			Extra u32s:\n");
		for(l = 0; l < 4; l++) {
			printf("				%04d: %08X\n", l, data[i].extra[l]);
		}
	}
	fseek(doaPtr, nextsecloc, SEEK_SET);
	return 0;
}

int parse_ptr(FILE* fp)
{
	doaPtr = fp;
	
	if(read_header()) {
		perror("Can't read header");
		return EXIT_FAILURE;
	}
	
	if(read_offset_table()) {
		perror("Can't read offset table");
		return EXIT_FAILURE;
	}
	
	if(parse_data()) {
		perror("Can't parse data");
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}
