/*
	SPARK - Dead or Alive Paradise Tool

Copyright (C) 2010              Giuseppe "The Lemon Man"
Copyright (C) 2010~2011         Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef MDL_PMD_H
#define MDL_PMD_H

#include <stdio.h>
#include "common.h"

#define MAGIC_PMD	(0x504D4400)

int parse_pmd(FILE* fp);

#endif
